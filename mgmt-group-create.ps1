# Author: Joey Brakefield
# Date: 07/14/20
# Name: Check to see if management group exists and, if not, create it with appropriate RHEDcloud Admin access
# Variables: make sure to export variables from BASH called:
#  $userName (the Service Principal App ID),
#  $tenant (the Tenant ID from Azure Active Directory), and
#  $password (The service principal secret) so that you can connect with the service principal


$tenantId = $env:tenant
$svcPrincipal = $env:userName
$passwd = ConvertTo-SecureString $env:password -AsPlainText -Force
$DevMGTier1 = "Dev-MG"
$ProdMGTier1 = "Prod-MG"
$HIPAATier2 = "HIPAA-MG"
$NonHIPAAPTier2 = "Non-HIPAA-MG"
$DevMGTier1Display = "Development Environment Management Group" 
$ProdMGTier1Display = "Production Environment Management Group"
$HIPAATier2Display = "HIPAA Management Group"
$NonHIPAATier2Display = "Non-HIPAA Management Group"

$Tier1CoreInfraMG = "RHEDcloud-Core"
$Tier1CoreInfraMGDisplay = "RHEDcloud Administrative Management Group"
$Tier2CoreProdMG = "RHEDcloud-Core-Prod"
$Tier2CoreDevMG = "RHEDcloud-Core-Dev"
$Tier2CoreProdMGDisplay = "RHEDcloud Administration Production Management Group"
$Tier2CoreDevMGDisplay = "RHEDcloud Administration Development Management Group"
  
$RHEDcloudAdminOID = "a8319c78-8c01-40ef-a0e5-275c09367c18"
$pscredential = New-Object System.Management.Automation.PSCredential($svcPrincipal, $passwd)




if (Get-Module -ListAvailable -Name Az.Accounts) {
    Write-Host "Module exists"
    Import-Module -Name az.Accounts -Force
} else {
    Install-Module -Name Az.Accounts -Force
    Import-Module -Name Az.Accounts -Force
}

if (Get-Module -ListAvailable -Name Az.Blueprint) {
    Write-Host "Module exists"
    Import-Module -Name az.Blueprint -Force
} else {
    Write-Host "Installing and importing module"
    Install-Module -Name Az.Blueprint -Force
    Import-Module -Name Az.Blueprint -Force
}

if (Get-Module -ListAvailable -Name Az.Resources) {
    Write-Host "Module exists"
    Import-Module -Name Az.Resources -Force

} else {
    Write-Host "Installing and importing module"
    Install-Module -Name Az.Resources -Force
    Import-Module -Name Az.Resources -Force
}

# Connect to Azure using a service principal user account
Connect-AzAccount -ServicePrincipal -Credential $pscredential -Tenant $tenantId
$cont = get-azcontext

# Export how we're connecting to Azure
Write-Host "Connecting to Azure using the following context: `n`n" 
$cont


# TODO: Add enviornment to the Management Group ID section
# Check to see if DEV + Child Management Groups Exist

if (Get-AzManagementGroup -GroupName $DevMGTier1){
    Write-Host "Development Management Group Exists. Continuing to next step"
} else { 
    $mgEnv = "Dev-"
    New-AzManagementGroup -GroupName $DevMGTier1 -DisplayName $DevMGTier1Display 

    # Create Child HIPAA & Non-HIPAA MGs under Dev
    New-AzManagementGroup -GroupName $mgEnv$HIPAATier2 -DisplayName $mgEnv$HIPAATier2Display -ParentObject (Get-AzManagementGroup -GroupName $DevMGTier1)
    New-AzManagementGroup -GroupName $mgEnv$NonHIPAAPTier2 -DisplayName $mgEnv$NonHIPAATier2Display -ParentObject (Get-AzManagementGroup -GroupName $DevMGTier1)
    # Bug in New-AzRoleAssignment at MG level. Will submit --JB 07/14/20
    
    # New-AzRoleAssignment -Scope "/providers/Microsoft.Management/managementGroups/$HIPAAMG" -RoleDefinitionName Owner -ObjectId $RHEDcloudAdminOID
    az role assignment create --role Owner --assignee $RHEDcloudAdminOID --scope /providers/Microsoft.Management/managementGroups/$DevMGTier1
    az role assignment create --role Owner --assignee $RHEDcloudAdminOID --scope /providers/Microsoft.Management/managementGroups/$DevMGTier1/$mgEnv$HIPAATier2
    az role assignment create --role Owner --assignee $RHEDcloudAdminOID --scope /providers/Microsoft.Management/managementGroups/$DevMGTier1/$mgEnv$NonHIPAAPTier2
}

    


# Check to see if Non-HIPAA Management Group exists
if (Get-AzManagementGroup -GroupName $ProdMGTier1){
    Write-Host "Production Management Group Exists. Continuing to next step"
} else {

    $mgEnv="Prod-"
    New-AzManagementGroup -GroupName $ProdMGTier1 -DisplayName $ProdMGTier1Display
    
    # Create Child HIPAA & Non-HIPAA MGs under Prod
    New-AzManagementGroup -GroupName $mgEnv$HIPAATier2 -DisplayName $mgEnv$HIPAATier2Display -ParentObject (Get-AzManagementGroup -GroupName $ProdMGTier1)
    New-AzManagementGroup -GroupName $mgEnv$NonHIPAAPTier2 -DisplayName $mgEnv$NonHIPAATier2Display -ParentObject (Get-AzManagementGroup -GroupName $ProdMGTier1)
   # Bug in New-AzRoleAssignment at MG level. Will submit --JB 07/14/20
   # New-AzRoleAssignment -Scope /providers/Microsoft.Management/managementGroups/$NonHIPAAMG -RoleDefinitionName Owner -ObjectId $RHEDcloudAdminOID
   az role assignment create --role Owner --assignee $RHEDcloudAdminOID --scope /providers/Microsoft.Management/managementGroups/$ProdMGTier1
   az role assignment create --role Owner --assignee $RHEDcloudAdminOID --scope /providers/Microsoft.Management/managementGroups/$ProdMGTier1/$mgEnv$HIPAATier2
   az role assignment create --role Owner --assignee $RHEDcloudAdminOID --scope /providers/Microsoft.Management/managementGroups/$ProdMGTier1/$mgEnv$NonHIPAAPTier2
}

# TODO: Create the If/Else for RHEDcloud-Admin-MG
if (Get-AzManagementGroup -GroupName $Tier1CoreInfraMG){
    Write-Host "Non-HIPAA Management Group Exists. Continuing to next step"
} else {
    New-AzManagementGroup -GroupName $Tier1CoreInfraMG -DisplayName $Tier1CoreInfraMGDisplay
    
    # Create Child HIPAA & Non-HIPAA MGs under Prod
    New-AzManagementGroup -GroupName $Tier2CoreProdMG -DisplayName $Tier2CoreProdMGDisplay -ParentObject (Get-AzManagementGroup -GroupName $Tier1CoreInfraMG)
    New-AzManagementGroup -GroupName $Tier2CoreDevMG -DisplayName $Tier2CoreDevMGDisplay -ParentObject (Get-AzManagementGroup -GroupName $Tier1CoreInfraMG)
   # Bug in New-AzRoleAssignment at MG level. Will submit --JB 07/14/20
   # New-AzRoleAssignment -Scope /providers/Microsoft.Management/managementGroups/$NonHIPAAMG -RoleDefinitionName Owner -ObjectId $RHEDcloudAdminOID
   az role assignment create --role Owner --assignee $RHEDcloudAdminOID --scope /providers/Microsoft.Management/managementGroups/$Tier1CoreInfraMG
   az role assignment create --role Owner --assignee $RHEDcloudAdminOID --scope /providers/Microsoft.Management/managementGroups/$Tier1CoreInfraMG/$Tier2CoreProdMG
   az role assignment create --role Owner --assignee $RHEDcloudAdminOID --scope /providers/Microsoft.Management/managementGroups/$Tier1CoreInfraMG/$Tier2CoreDevMG
}


Write-Host "Mangement Group Creation/Updates Complete"